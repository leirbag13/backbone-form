/*global define*/

define([
    'jquery',
    'backbone'
], function ($, Backbone) {
    'use strict';

    var FormRouter = Backbone.Router.extend({
        routes: {
        }

    });

    return FormRouter;
});
