/*global define*/

define([
    'underscore',
    'backbone',
    'models/form'
], function (_, Backbone, FormModel) {
    'use strict';

    var FormCollection = Backbone.Collection.extend({
        model: FormModel
    });

    return FormCollection;
});
